<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    public function run()
    {
        DB::table('quem_somos')->insert([
            'titulo' => '',
            'texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
        ]);
    }
}
