<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class FormasDePagamento extends Model
{
    protected $table = 'formas_de_pagamento';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 420,
            'height' => 420,
            'path'   => 'assets/img/formas-de-pagamento/'
        ]);
    }

}
