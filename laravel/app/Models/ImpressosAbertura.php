<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ImpressosAbertura extends Model
{
    protected $table = 'impressos_abertura';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 420,
            'height' => 420,
            'path'   => 'assets/img/impressos-abertura/'
        ]);
    }

}
