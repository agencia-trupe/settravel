<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImpressosRequest;
use App\Http\Controllers\Controller;

use App\Models\Impresso;

class ImpressosController extends Controller
{
    public function index()
    {
        $registros = Impresso::ordenados()->get();

        return view('painel.impressos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.impressos.create');
    }

    public function store(ImpressosRequest $request)
    {
        try {

            $input = $request->all();

            $input['arquivo'] = Impresso::uploadArquivo();

            Impresso::create($input);

            return redirect()->route('painel.impressos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Impresso $registro)
    {
        return view('painel.impressos.edit', compact('registro'));
    }

    public function update(ImpressosRequest $request, Impresso $registro)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = Impresso::uploadArquivo();
            }

            $registro->update($input);

            return redirect()->route('painel.impressos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Impresso $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.impressos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
