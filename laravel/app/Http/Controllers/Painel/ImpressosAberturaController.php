<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImpressosAberturaRequest;
use App\Http\Controllers\Controller;

use App\Models\ImpressosAbertura;

class ImpressosAberturaController extends Controller
{
    public function index()
    {
        $registro = ImpressosAbertura::first();

        return view('painel.impressos-abertura.edit', compact('registro'));
    }

    public function update(ImpressosAberturaRequest $request, ImpressosAbertura $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ImpressosAbertura::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.impressos-abertura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
