<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FormasDePagamentoRequest;
use App\Http\Controllers\Controller;

use App\Models\FormasDePagamento;

class FormasDePagamentoController extends Controller
{
    public function index()
    {
        $registro = FormasDePagamento::first();

        return view('painel.formas-de-pagamento.edit', compact('registro'));
    }

    public function update(FormasDePagamentoRequest $request, FormasDePagamento $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = FormasDePagamento::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.formas-de-pagamento.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
