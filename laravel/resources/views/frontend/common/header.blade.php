    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav>
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>
