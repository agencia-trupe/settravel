<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>
	<li @if(str_is('painel.impressos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.impressos.index') }}">Impressos</a>
	</li>
	<li @if(str_is('painel.formas-de-pagamento*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.formas-de-pagamento.index') }}">Formas de Pagamento</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
