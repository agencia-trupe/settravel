@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Impressos /</small> Abertura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.impressos-abertura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.impressos-abertura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
