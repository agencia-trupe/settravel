@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Impressos /</small> Adicionar Impresso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.impressos.store', 'files' => true]) !!}

        @include('painel.impressos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
