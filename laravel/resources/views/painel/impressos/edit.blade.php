@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Impressos /</small> Editar Impresso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.impressos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.impressos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
