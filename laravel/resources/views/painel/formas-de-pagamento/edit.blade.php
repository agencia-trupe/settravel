@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Formas de Pagamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.formas-de-pagamento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.formas-de-pagamento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
